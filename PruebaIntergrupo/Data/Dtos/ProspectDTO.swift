//
//  ProspectDTO.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import ObjectMapper

class ProspectDTO: Mappable {
    
    var id: String?
    var name: String?
    var surname: String?
    var telephone: String?
    var schProspectIdentification: String?
    var address: String?
    var createdAt: String?
    var updatedAt: String?
    var statusCd: Int = 0
    var zoneCode: String?
    var neighborhoodCode: String?
    var cityCode: String?
    var sectionCode: String?
    var roleId: Int = 0
    var appointableId: Int = 0
    var rejectedObservation: Int = 0
    var observation: String?
    var isDisable: Bool = false
    var isVisited: Bool = false
    var isCallcenter: Bool = false
    var isAcceptSearch: Bool = false
    var campaignCode: String?
    var userId: Int = 0
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        surname <- map["surname"]
        telephone <- map["telephone"]
        schProspectIdentification <- map["schProspectIdentification"]
        address <- map["address"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        statusCd <- map["statusCd"]
        zoneCode <- map["zoneCode"]
        neighborhoodCode <- map["neighborhoodCode"]
        cityCode <- map["cityCode"]
        sectionCode <- map["sectionCode"]
        roleId <- map["roleId"]
        appointableId <- map["appointableId"]
        rejectedObservation <- map["rejectedObservation"]
        observation <- map["observation"]
        isDisable <- map["disable"]
        isVisited <- map["visited"]
        isCallcenter <- map["callcenter"]
        isAcceptSearch <- map["acceptSearch"]
        campaignCode <- map["campaignCode"]
        userId <- map["userId"]
    }
    
    
}
