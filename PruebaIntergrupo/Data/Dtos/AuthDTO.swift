//
//  AuthDTO.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import ObjectMapper

class AuthDTO: Mappable {
    
    var authToken: String?
    var zone: String?
    var email: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        authToken <- map["authToken"]
        zone <- map["zone"]
        email <- map["email"]
    }
}
