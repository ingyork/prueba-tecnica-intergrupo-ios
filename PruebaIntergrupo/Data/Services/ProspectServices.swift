//
//  ProspectServices.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RealmSwift

class ProspectServices {
    
    static func login(username: String, password: String, isRemember: Bool) {
        let dbHelper = DBHelper()
        if NetworkUtils.isConnectedToInternet {
            let endpoint = "http://directotesting.igapps.co/application/login"
            let parameters: Parameters = [
                "email": username,
                "password": password
            ]
            
            Alamofire.request(endpoint, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: nil).responseObject { (response: DataResponse<AuthDTO>) in
                let data = response.result.value
                if let data = data {
                    var authEntity = AuthEntity()
                    authEntity.id = 1
                    authEntity.authToken = data.authToken
                    authEntity._zone = data.zone
                    authEntity.email = data.email
                    authEntity.isRemember = isRemember
                    dbHelper.insertRow(authEntity)
                    ProspectViewModel.prospectSubject.asObserver().onNext(data)
                }
                
            }
        }
    }
    
    static func isRememberSession() -> Bool{
        
        var isRemember = false
        let db = DBHelper()
        if db.isNotEmptyTable(obj: AuthEntity.self) {
            if let auth: AuthEntity = db.getRows()?.first {
                isRemember = auth.isRemember
            }
        }
        return isRemember
    }

    
    static func getProspect(id: String) -> ProspectDTO?{
        var response: ProspectDTO?
        let db = DBHelper()
        if db.isNotEmptyTable(obj: ProspectEntity.self) {
            if let entity: ProspectEntity = db.getRowBy(id: id) {
                response = DBUtil.mapProspectToDTO(prospect: entity)
            }
        }
        return response
    }
    
    static func listProspect(){
    
        let db = DBHelper()
        if db.isEmptyTable(obj: ProspectEntity.self) {
            
            let endpoint = "http://directotesting.igapps.co/sch/prospects.json"
            let headers = [
                "token": "9FQn5gzKA9k2LWyBMZtM"
            ]
            Alamofire.request(endpoint, headers: headers).responseArray { (response: DataResponse<[ProspectDTO]>) in
                if let data = response.result.value {
                    
                    let pentities = DBUtil.mapProspectListToEntity(prospects: data)
                    db.insertRows(pentities)
                    ProspectViewModel.prospectsSubject.asObserver().onNext(data)
                }
            }
            
        }
        else {
            let entities: Results<ProspectEntity>? = db.getRows()
            let data = DBUtil.mapProspectListToDTO(prospects: entities)
            ProspectViewModel.prospectsSubject.asObserver().onNext(data)
        }
        
    }
    
    
    static func updateProspect(prospect: ProspectDTO) {
        let entity = DBUtil.mapProspectToEntity(prospect: prospect)
        DBHelper().updateRow(entity)
    }
    
    static func logout() -> Bool{
        return DBHelper().deleteAll(T: AuthEntity.self)
    }
}
