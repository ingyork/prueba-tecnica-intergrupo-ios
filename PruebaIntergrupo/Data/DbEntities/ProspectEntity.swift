//
//  ProspectEntity.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import RealmSwift

class ProspectEntity: Object {

    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var surname: String?
    @objc dynamic var telephone: String?
    @objc dynamic var schProspectIdentification: String?
    @objc dynamic var address: String?
    @objc dynamic var createdAt: String?
    @objc dynamic var updatedAt: String?
    @objc dynamic var statusCd: Int = 0
    @objc dynamic var zoneCode: String?
    @objc dynamic var neighborhoodCode: String?
    @objc dynamic var cityCode: String?
    @objc dynamic var sectionCode: String?
    @objc dynamic var roleId: Int = 0
    @objc dynamic var appointableId: Int = 0
    @objc dynamic var rejectedObservation: Int = 0
    @objc dynamic var observation: String?
    @objc dynamic var isDisable: Bool = false
    @objc dynamic var isVisited: Bool = false
    @objc dynamic var isCallcenter: Bool = false
    @objc dynamic var isAcceptSearch: Bool = false
    @objc dynamic var campaignCode: String?
    @objc dynamic var userId: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
