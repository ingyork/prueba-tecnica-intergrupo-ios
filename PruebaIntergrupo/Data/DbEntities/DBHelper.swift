//
//  DBHelper.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Realm
import RealmSwift

class DBHelper {
    
    var realm: Realm!
    
    init() {
        realm = try! Realm()
    }
    
    func insertRow<T>(_ item: T){
        try! self.realm.write {
            self.realm.create(T.self as! Object.Type, value: item, update: true)
        }
    }
    
    func insertRows<T>(_ array: [T]){
        try! self.realm.write {
            array.forEach({
                self.realm.create(T.self as! Object.Type, value: $0, update: true)
            })
        }
    }
    
    func updateRow<T>(_ item: T){
        try! self.realm.write {
            self.realm.add(item as! Object, update: true)
        }
    }
    
    func deleteAll(T: Object.Type) -> Bool{
        var status = false
        try! self.realm.write {
            let ents = realm.objects(T.self)
            realm.delete(ents)
            status = true
        }
        return status
    }
    
    func getRows<T: Object>() -> Results<T>? {
        return realm.objects(T.self)
    }
    
    func getRowBy<T: Object>(id: Int) -> T? {
        return realm.objects(T.self).filter("id = \(id)").first
    }
    
    func getRowBy<T: Object>(id: Int64) -> T? {
        return realm.objects(T.self).filter("id = \(id)").first
    }
    
    func getRowBy<T: Object>(id: String) -> T? {
        return realm.objects(T.self).filter("id = '\(id)'").first
    }
    
    func getRowsByFilter<T: Object>(filter: String) -> Results<T>? {
        return realm.objects(T.self).filter(filter)
    }
    
    func getRowsByFilterArray<T: Object>(filterArray: [String]) -> Results<T>? {
        
        var predicates = [NSPredicate]()
        filterArray.forEach { (filter) in
            predicates.append(NSPredicate(format: "id = %@", filter))
        }
        let query = NSCompoundPredicate(type: .or, subpredicates: predicates)
        return realm.objects(T.self).filter(query)
    }
    
    
    func getRowsByFilterArray<T: Object>(filterArray: [Int]) -> Results<T>? {
        
        var predicates = [NSPredicate]()
        filterArray.forEach { (filter) in
            predicates.append(NSPredicate(format: "id = %@", NSNumber(value: filter)))
        }
        let query = NSCompoundPredicate(type: .or, subpredicates: predicates)
        
        return realm.objects(T.self).filter(query)
    }
    
    func isEmptyTable<T>(obj: T) -> Bool {
        return realm.objects(obj.self as! Object.Type).isEmpty
    }
    
    func isNotEmptyTable<T>(obj: T) -> Bool {
        return !realm.objects(obj.self as! Object.Type).isEmpty
    }
}
