//
//  AuthEntity.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//
import RealmSwift

class AuthEntity: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var authToken: String?
    @objc dynamic var _zone: String?
    @objc dynamic var email: String?
    @objc dynamic var isRemember: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
