//
//  ProspectViewModel.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import RxSwift

class ProspectViewModel {
    
    static var prospectSubject = PublishSubject<Any>()
    static var prospectsSubject = PublishSubject<[Any]>()
    
    static func dispatchLogin(username: String, password: String, isRemember: Bool) {
        ProspectServices.login(username: username, password: password, isRemember: isRemember)
    }
    
    static func dispathListProspect() {
        ProspectServices.listProspect()
    }
    
    static func isRememberSession() -> Bool{
        return ProspectServices.isRememberSession()
    }
    
    static func getProspectById(id: String) -> ProspectDTO?{
        return ProspectServices.getProspect(id: id)
    }
    
    static func updateProspect(prospect: ProspectDTO) {
        ProspectServices.updateProspect(prospect: prospect)
    }
    
    static func logout() -> Bool{
        return ProspectServices.logout()
    }
}
