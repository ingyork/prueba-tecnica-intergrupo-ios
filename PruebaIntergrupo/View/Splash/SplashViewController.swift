//
//  SplashViewController.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeStackNavigationWithoutSelf()
        navigationController?.navigationBar.isHidden = true
    }
    
    func removeStackNavigationWithoutSelf() {
        var index = 0
        navigationController?.viewControllers.forEach({ (controller) in
            if !(controller is SplashViewController) {
                navigationController?.viewControllers.remove(at: index)
            }
            index+=1
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initAnimation()
    }
    

    func initAnimation(){
        UIView.animate(withDuration: 0.1, delay: 1, options: [], animations: {
            self.logo.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { (Bool) in
            self.secondAnimation()
        }
    }
    
    func secondAnimation(){
        UIView.animate(withDuration: 0.1, animations: {
            self.logo.transform = .identity
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                if ProspectViewModel.isRememberSession() {
                    self.navigateToProspectList()
                }
                else {
                    self.navigateToLogin()
                }
            }
        }
    }
    
    func navigateToLogin(){
        navigationController?.pushViewController(LoginViewController(), animated: true)
        navigationController?.viewControllers.remove(at: 0)
    }
    
    func navigateToProspectList() {
        let controller = ProspectListViewController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(controller, animated: true)
        navigationController?.viewControllers.remove(at: 0)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
