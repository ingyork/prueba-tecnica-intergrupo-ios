//
//  LoginViewController.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var subscriptions = [Disposable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDelegateObservable.subscribe(onNext: { (state) in
            switch state {
            case .willEnterBackground:
                self.unsubscribe()
            case .willEnterForeground:
                self.initSubscribe()
            default: break
            }
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initSubscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribe()
    }

    @IBAction func actionLogin(_ sender: Any) {
        let username = txtUser.text
        let password = txtPassword.text
        if let username = username, let password = password {
            ProspectViewModel.dispatchLogin(username: username, password: password, isRemember: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController {
    
    func initSubscribe(){
        subscriptions.append(
            ProspectViewModel.prospectSubject.asObservable().subscribe(onNext: { [weak self] (response) in
                
                if let _ = (response as? AuthDTO) {
                    let controller = ProspectListViewController(collectionViewLayout: UICollectionViewFlowLayout())
                    self?.navigationController?.pushViewController(controller, animated: true)
                    self?.navigationController?.viewControllers.remove(at: 0)
                }
            })
        )
    }
    
    func unsubscribe(){
        subscriptions.forEach({ $0.dispose() })
    }
}
