//
//  ProspectDetailViewController.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class ProspectDetailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var statusCode: UITextField!
    @IBOutlet weak var zoneCode: UITextField!
    @IBOutlet weak var phone: UITextField!
    
    var prospectId: String?
    var currentProspect: ProspectDTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Prospect Detail"
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "logout_icon"), style: .plain, target: self, action: #selector(actionLogout))
        self.navigationItem.rightBarButtonItem = btn
        
        initTextField()
        loadData()
    }
    
    func initTextField() {
        name.delegate = self
        surname.delegate = self
        address.delegate = self
        statusCode.delegate = self
        zoneCode.delegate = self
        phone.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    private func loadData(){
        if let id = prospectId {
            currentProspect = ProspectViewModel.getProspectById(id: id)
            if let prospect = currentProspect {
                name.text = prospect.name
                surname.text = prospect.surname
                address.text = prospect.address
                statusCode.text = "\(prospect.statusCd)"
                zoneCode.text = prospect.zoneCode
                phone.text = prospect.telephone
            }
        }
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        
        if let name = name.text, let surname = surname.text, let address = address.text,
            let statusCode = statusCode.text, let zoneCode = zoneCode.text, let phone = phone.text {
            currentProspect?.name = name
            currentProspect?.surname = surname
            currentProspect?.address = address
            currentProspect?.statusCd = Int(statusCode) ?? 0
            currentProspect?.zoneCode = zoneCode
            currentProspect?.telephone = phone
            
            if let p = currentProspect {
                ProspectViewModel.updateProspect(prospect: p)
                
                let alert = UIAlertController(title: "Mensaje", message: "Se ha actualizado el registro correctamente!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    @objc func actionLogout() {
        let alert = UIAlertController(title: "Advertencia!", message: "Desea cerrar la sesión?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "SI", style: .default, handler: { (UIAlertAction) in
            if ProspectViewModel.logout() {
                self.navigationController?.pushViewController(SplashViewController(), animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
