//
//  ProspectListViewController.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit
import RxSwift

private let reuseIdentifier = "Cell"

class ProspectListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var subscriptions = [Disposable]()
    var data = [ProspectDTO]()
    var progress: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Prospect List"
        navigationController?.navigationBar.isHidden = false
        
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "logout_icon"), style: .plain, target: self, action: #selector(actionLogout))
        self.navigationItem.rightBarButtonItem = btn
        
        if let view = self.navigationController?.view {
            progress = UIView.displaySpinner(onView: view)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UINib(nibName: "ProspectViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        appDelegateObservable.subscribe(onNext: { (state) in
            switch state {
            case .willEnterBackground:
                self.unsubscribe()
            case .willEnterForeground:
                self.initSubscribe()
            default: break
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.backgroundColor = .white
        initSubscribe()
        ProspectViewModel.dispathListProspect()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribe()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return data.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProspectViewCell
    
        cell.index = indexPath.row
        cell.delegate = self
        
        let pr = data[indexPath.row]
        cell.phone.text = pr.telephone
        cell.identification.text = pr.schProspectIdentification
        if let name = pr.name, let surname = pr.surname {
            cell.fullName.text = "\(name) \(surname)"
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 160)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    @objc func actionLogout() {
        let alert = UIAlertController(title: "Advertencia!", message: "Desea cerrar la sesión?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "SI", style: .default, handler: { (UIAlertAction) in
            if ProspectViewModel.logout() {
                self.navigationController?.pushViewController(SplashViewController(), animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
}


extension ProspectListViewController: ProspectDelegate {
    func initSubscribe(){
        subscriptions.append(
            ProspectViewModel.prospectsSubject.asObservable().subscribe(onNext: { [weak self] (response) in
                if let data = (response as? [ProspectDTO]) {
                    self?.data = data
                    self?.collectionView.reloadData()
                    
                    if let progress = self?.progress {
                        UIView.removeSpinner(spinner: progress)
                    }
                }
            })
        )
    }
    
    func unsubscribe(){
        subscriptions.forEach({ $0.dispose() })
    }
    
    func selectCell(index: Int) {
        let controller = ProspectDetailViewController()
        controller.prospectId = data[index].id
        navigationController?.pushViewController(controller, animated: true)
    }
}
