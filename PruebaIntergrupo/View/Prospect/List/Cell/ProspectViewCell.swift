//
//  ProspectViewCell.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/14/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class ProspectViewCell: UICollectionViewCell {

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var identification: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    
    var delegate: ProspectDelegate?
    var index: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func actionSelectCell(_ sender: Any) {
        delegate?.selectCell(index: index)
    }
    
}


protocol ProspectDelegate {
    func selectCell(index: Int)
}
