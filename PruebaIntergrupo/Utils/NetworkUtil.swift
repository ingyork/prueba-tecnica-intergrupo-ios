//
//  NetworkUtil.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Alamofire

class NetworkUtils {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
