//
//  DBUtil.swift
//  PruebaIntergrupo
//
//  Created by Jorge Castro on 10/15/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import RealmSwift

class DBUtil {
    
    static func mapProspectListToEntity(prospects: [ProspectDTO]) -> [ProspectEntity] {
        var pentities = [ProspectEntity]()
        prospects.forEach { (p) in
            let pre = ProspectEntity()
            pre.id = p.id
            pre.name = p.name
            pre.surname = p.surname
            pre.telephone = p.telephone
            pre.schProspectIdentification = p.schProspectIdentification
            pre.address = p.address
            pre.createdAt = p.createdAt
            pre.updatedAt = p.updatedAt
            pre.statusCd = p.statusCd
            pre.zoneCode = p.zoneCode
            pre.neighborhoodCode = p.neighborhoodCode
            pre.cityCode = p.cityCode
            pre.sectionCode = p.sectionCode
            pre.roleId = p.roleId
            pre.appointableId = p.appointableId
            pre.rejectedObservation = p.rejectedObservation
            pre.observation = p.observation
            pre.isDisable = p.isDisable
            pre.isVisited = p.isVisited
            pre.isCallcenter = p.isCallcenter
            pre.isAcceptSearch = p.isAcceptSearch
            pre.campaignCode = p.campaignCode
            pre.userId = p.userId
            pentities.append(pre)
        }
        return pentities
    }
    
    static func mapProspectToDTO(prospect p: ProspectEntity) -> ProspectDTO {
        let pre = ProspectDTO()
        pre.id = p.id
        pre.name = p.name
        pre.surname = p.surname
        pre.telephone = p.telephone
        pre.schProspectIdentification = p.schProspectIdentification
        pre.address = p.address
        pre.createdAt = p.createdAt
        pre.updatedAt = p.updatedAt
        pre.statusCd = p.statusCd
        pre.zoneCode = p.zoneCode
        pre.neighborhoodCode = p.neighborhoodCode
        pre.cityCode = p.cityCode
        pre.sectionCode = p.sectionCode
        pre.roleId = p.roleId
        pre.appointableId = p.appointableId
        pre.rejectedObservation = p.rejectedObservation
        pre.observation = p.observation
        pre.isDisable = p.isDisable
        pre.isVisited = p.isVisited
        pre.isCallcenter = p.isCallcenter
        pre.isAcceptSearch = p.isAcceptSearch
        pre.campaignCode = p.campaignCode
        pre.userId = p.userId
        return pre
    }
    
    static func mapProspectToEntity(prospect p: ProspectDTO) -> ProspectEntity {
        let pre = ProspectEntity()
        pre.id = p.id
        pre.name = p.name
        pre.surname = p.surname
        pre.telephone = p.telephone
        pre.schProspectIdentification = p.schProspectIdentification
        pre.address = p.address
        pre.createdAt = p.createdAt
        pre.updatedAt = p.updatedAt
        pre.statusCd = p.statusCd
        pre.zoneCode = p.zoneCode
        pre.neighborhoodCode = p.neighborhoodCode
        pre.cityCode = p.cityCode
        pre.sectionCode = p.sectionCode
        pre.roleId = p.roleId
        pre.appointableId = p.appointableId
        pre.rejectedObservation = p.rejectedObservation
        pre.observation = p.observation
        pre.isDisable = p.isDisable
        pre.isVisited = p.isVisited
        pre.isCallcenter = p.isCallcenter
        pre.isAcceptSearch = p.isAcceptSearch
        pre.campaignCode = p.campaignCode
        pre.userId = p.userId
        return pre
    }
    
    static func mapProspectListToDTO(prospects: Results<ProspectEntity>?) -> [ProspectDTO] {
        var pdtos = [ProspectDTO]()
        prospects?.forEach { (p) in
            let pre = ProspectDTO()
            pre.id = p.id
            pre.name = p.name
            pre.surname = p.surname
            pre.telephone = p.telephone
            pre.schProspectIdentification = p.schProspectIdentification
            pre.address = p.address
            pre.createdAt = p.createdAt
            pre.updatedAt = p.updatedAt
            pre.statusCd = p.statusCd
            pre.zoneCode = p.zoneCode
            pre.neighborhoodCode = p.neighborhoodCode
            pre.cityCode = p.cityCode
            pre.sectionCode = p.sectionCode
            pre.roleId = p.roleId
            pre.appointableId = p.appointableId
            pre.rejectedObservation = p.rejectedObservation
            pre.observation = p.observation
            pre.isDisable = p.isDisable
            pre.isVisited = p.isVisited
            pre.isCallcenter = p.isCallcenter
            pre.isAcceptSearch = p.isAcceptSearch
            pre.campaignCode = p.campaignCode
            pre.userId = p.userId
            pdtos.append(pre)
        }
        return pdtos
    }
    
}
